if __name__ == "__main__":
    print("PLACAS DE VEHICULOS")
    print("-------------------")
    import re

    placa = input("Placa a evaluar:   ")
    if re.match("^T([0-9])([0-9])([0-9])([0-9])([0-9]$)", placa):
        print("\nPlaca de Taxi")

    elif re.match("(^M)(B)([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Metro Bus")

    elif re.match("(^B)([0-9])([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Bus")

    elif re.match("(^C)(D)([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Cuerpo Diplomatico")

    elif re.match("(^M)([0-9])([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Moto")

    elif re.match("(^M)([0-9])([0-9])([0-9])([0-9]$)", placa):
        print("\nPlaca de Moto")

    elif re.match("(^D)([0-9])([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Automovil de Prueba")

    elif re.match("(^P)(R)([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Periodista")

    elif re.match("(^C)(C)([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Cuerpo Consular")

    elif re.match("(^E)([0-9])([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Juez o Fiscal")

    elif re.match("(^[A-Z_0-9])([A-Z-0-9])([0-9])([0-9])([0-9])([0-9])([0-9]$)",placa):
        print("\nPlaca de Carro Particular")

    else:
        print("No coincide con la Base de Datos")


